package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by msmon on 1/12/2017.
 */
public class Swordsman  {
    public int hitPoint = 100;
    public int damages = 5;

    public static Swordsman swordMan = new Swordsman();

    List<Swordsman> swordList = new ArrayList<Swordsman>();
    List<Viking> vkList = new ArrayList<Viking>();
    int swDmgs=0;
    int vkDmgs=0;
    int swHitPts = 0;
    int vkHitPts =0;


    public int getHitPoint() {
        return hitPoint;
    }


    public void setHitPoint(int hitPoint) {
        this.hitPoint = hitPoint;
    }

    public int getDamages() {
        return damages;
    }

    public void setDamages(int damages) {
        this.damages = damages;
    }

    public Swordsman() {

    }

    public Swordsman(int hitPoint, int damages) {
        this.hitPoint = hitPoint;
        this.damages = damages;
    }

    public static Swordsman getInstance(){
        if(swordMan != null){
            swordMan = new Swordsman();
        }
        return swordMan;
    }

    public void engage (Viking vkg){
        swordList.add(new Swordsman(100,5));
        vkList.add(new Viking(120,6));
        for(Swordsman swd : swordList){
            swDmgs= swd.getDamages();
            swHitPts = swd.getHitPoint();
        }
        for(Viking vk:vkList){
            vkDmgs=vk.getDamages();
            vkHitPts=vk.getHitPoint();
        }

        while(swHitPts >= swDmgs){
            swHitPts = swHitPts- vkDmgs;
            vkHitPts = vkHitPts -swDmgs;
        }
        if(swHitPts < swDmgs){
            swordMan.setHitPoint(0);
        }
        vkg.setHitPoint(vkHitPts);
        hitPoints();

    }

    public Integer hitPoints(){
        return new Integer(swordMan.getHitPoint());

    }




}
