package com.nespresso.sofa.recruitement.tournament.fighter;

/**
 * Created by msmon on 1/12/2017.
 */
public class Viking {
    int hitPoint = 120;
    int damages = 6;
    public static Viking vkg = new Viking();
    public Viking() {
    }

    public int getHitPoint() {
        return hitPoint;
    }

    public void setHitPoint(int hitPoint) {
        this.hitPoint = hitPoint;
    }

    public int getDamages() {
        return damages;
    }

    public void setDamages(int damages) {
        this.damages = damages;
    }

    public Viking(int hitPoint, int damages) {
        this.hitPoint = hitPoint;
        this.damages = damages;
    }

    public static Viking getInstance(){
        if(vkg != null){
            vkg = new Viking();
        }
        return vkg;
    }


}
